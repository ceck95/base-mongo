const MongoClient = require('mongodb').MongoClient;
const config = require('config');
const BPromise = require('bluebird');
const Hoek = require('hoek');
const log = require('core-express').Log;

class MongoDB {
  constructor() {
    this.db = null;
  }

  get log() {
    return log;
  }

  pool() {
    Hoek.assert(config.mongo, 'Config mongo empty.Please check config');
    return new BPromise((resolve, reject) => {
      if (this.db) return resolve(this.db);
      let opts = config.mongo.options || {};
      opts.promiseLibrary = BPromise;
      const url = config.mongo.connection;

      return MongoClient.connect(url, opts)
        .then(db => {
          this.log.info(
            `Create connection mongo successfully with config: ${
              config.mongo.connection
            }`
          );
          this.db = db;
          return resolve(db);
        })
        .catch(err => {
          this.log.error(err);
          return reject(err);
        });
    });
  }
}

const ClassPool = new MongoDB();
module.exports = ClassPool.pool;
module.exports.ClassPool = ClassPool;
