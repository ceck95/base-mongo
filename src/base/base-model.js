const Hoek = require('hoek');

class ModelBase {

  get collectionName() {
    Hoek.assert(false, 'Cannot empty function get connections');
  }

}

module.exports = ModelBase;