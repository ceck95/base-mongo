const Hoek = require('hoek');
const BPromise = require('bluebird');
const helpers = require('helpers');
const Log = require('core-express').Log;
const helperAdapter = require('./helpers');
const pool = require('../connection/pool');

class BaseAdapter {
  static async instance(instanceName, nameClassModel, adapter) {
    if (instanceName.indexOf('Adapter') < 0)
      Hoek.assert(false, `${instanceName} name is not correct format`);

    const nameModel = helpers.String.toSnakeCase(
        instanceName.replace('Adapter', ''),
        {
          delimiter: '-'
        }
      ),
      classCurrent = require(`${process.cwd()}/models/${nameModel}`),
      modelClass = new classCurrent();
    const poolConnect = await pool.bind(pool.ClassPool)();
    return new adapter(modelClass, poolConnect);
  }

  constructor(model, pool) {
    this.model = model;
    this.log.info(`Collection name: ${model.collectionName}`);
    this.log.debug({
      columns: model
    });
    this.pool = pool;
  }

  get logName() {
    return 'MongoDB';
  }

  get log() {
    if (!this._log)
      this._log = Log.child({
        namespace: this.logName,
        adapter: this.constructor.name
      });

    return this._log;
  }

  insert(data, opts) {
    data = helperAdapter.formData(data, this.model);
    data = helperAdapter.checkKeyModelWithData(data, this.model);
    let functionName = 'insertOne';
    if (Array.isArray(data)) functionName = 'insertMany';
    return this.query(functionName, data)
      .then(result => {
        this.log.debug('Insert successfully');
        this.log.debug(`Rows inserted: ${result.insertedCount}`);
        this.log.debug('----------------------------------------');

        result = Array.isArray(data) ? result.ops : result.ops[0];
        result = helperAdapter.parseStringKey(result, {
          toCamelCase: true
        });
        return BPromise.resolve(result);
      })
      .catch(err => {
        return BPromise.reject(err);
      });
  }

  update(filter, data, opts = {}) {
    data = helperAdapter.formData(data, this.model, {
      update: true
    });
    data = helperAdapter.checkKeyModelWithData(data, this.model);
    filter = helperAdapter.parseParams(filter);
    filter = helperAdapter.checkKeyModelWithData(filter, this.model);
    let optUpdate = null;

    if (!opts.updateAllField) {
      data = {
        $set: data
      };
    } else {
      if (Object.keys(data).length !== Object.keys(this.model).length)
        Hoek.assert(
          false,
          'Active key updateAllField you need input data full field data such as model'
        );
    }

    if (opts.upsert)
      optUpdate = {
        upsert: true
      };
    let functionName = opts.updateMany ? 'updateMany' : 'updateOne',
      prom = this.query(functionName, filter, data);

    if (optUpdate && !opts.updateMany)
      prom = this.query(functionName, filter, data, optUpdate);

    return prom
      .then(result => {
        let resp = null;
        this.log.debug(
          `Update ${opts.updateMany ? 'many' : 'one'} successfully`
        );
        this.log.debug(`Row modified: ${result.modifiedCount}`);
        this.log.debug(`Row upserted: ${result.upsertedCount}`);
        this.log.debug(`Row matched: ${result.matchedCount}`);
        if (result.upsertedCount > 0) resp = result.upsertedId._id;
        this.log.debug('----------------------------------------');
        return BPromise.resolve(resp);
      })
      .catch(err => {
        return BPromise.reject(err);
      });
  }

  logHead(functionName, args) {
    this.log.debug('----------------------------------------');
    this.log.debug(`Query with function name ${functionName}`);
    this.log.debug(`Args with function name ${functionName}:`);
    this.log.debug({
      args: args
    });
  }

  query(functionName, ...args) {
    Hoek.assert(
      functionName,
      'functionName must not be null in query base adapter mongo'
    );
    return new BPromise((resolve, reject) => {
      this.logHead(functionName, args);

      const db = this.pool,
        collection = db.collection(this.model.collectionName);

      return collection[functionName](...args)
        .then(result => {
          const except = [
            'insertOne',
            'insertMany',
            'deleteMany',
            'updateOne',
            'updateMany'
          ];
          if (!except.includes(functionName)) {
            result = helperAdapter.parseStringKey(result, {
              toCamelCase: true
            });
          }
          return resolve(result);
        })
        .catch(err => {
          return reject(err);
        });
    });
  }

  getOne(params, opts) {
    opts = opts || {};
    if (opts.includes) {
      opts.limit = 1;
      return this.commonCursor(params, opts).then(query => {
        return query
          .toArray()
          .then(result => {
            this.logFoot('one', result);
            return BPromise.resolve(result[0]);
          })
          .catch(err => {
            return BPromise.reject(err);
          });
      });
    }

    if (opts.fields && Array.isArray(opts.fields) && opts.fields.length > 0) {
      let fieldObject = {};
      opts.fields.forEach(e => {
        fieldObject = {
          [e]: 1
        };
      });
      opts.fields = fieldObject;
      helperAdapter.checkKeyModelWithData(opts.fields, this.model);
    }
    params = helperAdapter.checkKeyModelWithData(params, this.model);
    const optsQuery = helperAdapter.toKeyOptions(opts, this.model);
    return this.query('findOne', params, optsQuery)
      .then(result => {
        result = result || {};
        this.logFoot('one', result);
        if (opts.checkNull) {
          if (helpers.Json.checkEmptyObject(result)) {
            return BPromise.reject({
              code: '202',
              source: 'record not found in mongo'
            });
          }
        }
        return BPromise.resolve(result);
      })
      .catch(err => {
        return BPromise.reject(err);
      });
  }

  logFoot(name, result) {
    this.log.debug(`Get ${name} successfully`);

    if (result) {
      if (
        typeof result === 'object' &&
        !helpers.Json.checkEmptyObject(result)
      ) {
        this.log.debug('Rows received: 1');
      } else if (Array.isArray(result))
        this.log.debug(`Rows received: ${result.length}`);
      else if (
        typeof result === 'object' &&
        helpers.Json.checkEmptyObject(result)
      ) {
        this.log.debug('Rows received: 0');
      } else this.log.debug(`Rows received: ${result}`);
    }
    this.log.debug('----------------------------------------');
  }

  getMany(params, opts = {}) {
    let show = true;
    if (opts.queryManyForPagination) {
      delete opts.queryManyForPagination;
      show = false;
    }

    return this.commonCursor(params, opts).then(query => {
      return query
        .toArray()
        .then(result => {
          if (show) this.logFoot('many', result);
          result = helperAdapter.parseStringKey(result, {
            toCamelCase: true
          });
          return BPromise.resolve(result);
        })
        .catch(err => {
          return BPromise.reject(err);
        });
    });
  }

  commonCursor(params, opts) {
    opts = opts || {};
    params = helperAdapter.parseParams(params);
    params = helperAdapter.checkKeyModelWithData(params, this.model);

    const db = this.pool,
      collection = db.collection(this.model.collectionName);

    if (params)
      if (opts.includes || opts.groupBy) {
        const args = helperAdapter.parseAggregate(params, opts, this.model);
        this.logHead('aggregate', args);
        return BPromise.resolve(collection.aggregate(args));
      }

    let query = collection.find(params);

    if (opts.skip) query = query.skip(opts.skip);
    if (opts.limit) query = query.limit(opts.limit);
    if (opts.sort)
      query = query.sort(helperAdapter.parseSort(opts.sort, this.model));

    this.logHead('find', {
      params: params,
      opts: opts
    });

    return BPromise.resolve(query);
  }

  delete(params, opts) {
    params = helperAdapter.checkKeyModelWithData(params, this.model);
    return this.query('deleteMany', params)
      .then(result => {
        return BPromise.resolve(result);
      })
      .catch(err => {
        return BPromise.reject(err);
      });
  }

  count(params, opts) {
    opts = opts || {};
    const funcNameCurrent = 'count';
    return this.commonCursor(params, opts).then(query => {
      if (opts.countAggregate) {
        delete opts.countAggregate;
        return query
          .toArray()
          .then(count => {
            this.logFoot(funcNameCurrent, count);
            return BPromise.resolve(count[0].result);
          })
          .catch(err => {
            return BPromise.reject(err);
          });
      }
      return query
        .count()
        .then(result => {
          this.logFoot(funcNameCurrent, result);
          return BPromise.resolve(result);
        })
        .catch(err => {
          return BPromise.reject(err);
        });
    });
  }

  getPagination(params, opts) {
    Hoek.assert(opts.paging, 'Cannot empty params paging');
    Hoek.assert(!opts.limit, 'Get pagination cannot exist param limit');
    const paging = opts.paging;
    return this.count(params, opts)
      .then(count => {
        const totalRows = count,
          pageSize = paging.pageSize > 0 ? paging.pageSize : totalRows,
          totalPages =
            totalRows % pageSize === 0
              ? totalRows / pageSize
              : Math.floor(totalRows / pageSize) + 1;

        let resp = {
          meta: {
            pageSize: pageSize,
            pageNumber: paging.pageNumber,
            totalPages: totalPages,
            total: totalRows
          }
        };

        const skip = (paging.pageNumber - 1) * pageSize || 0;
        if (skip > totalRows) {
          resp.data = [];
          resp.meta.count = 0;
          return BPromise.resolve(resp);
        }
        const optGetMany = Object.assign(
          {
            skip: skip,
            limit: opts.paging.pageSize,
            queryManyForPagination: true
          },
          opts
        );

        return this.getMany(params, optGetMany)
          .then(result => {
            resp.meta.count = result.length;
            resp.data = helperAdapter.parseStringKey(result, {
              toCamelCase: true
            });
            this.logFoot('pagination', result);
            return BPromise.resolve(resp);
          })
          .catch(err => {
            return BPromise.resolve(err);
          });
      })
      .catch(err => {
        return BPromise.reject(err);
      });
  }
}

module.exports = BaseAdapter;
