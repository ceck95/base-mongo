const Hoek = require('hoek');
const helpers = require('helpers');
const mongodb = require('mongodb');
const ObjectID = mongodb.ObjectID;

class HelperAdapter {
  static checkKeyModelWithData(data, model) {
    Hoek.assert(data, 'Data check cannot null adapter mongodb');
    Hoek.assert(model, 'Model check cannot null adapter mongodb');
    const keyModel = Object.keys(model),
      keyError = [],
      checkFunc = current => {
        Object.keys(current).forEach(e => {
          if (e !== 'id')
            if (!keyModel.includes(e)) {
              keyError.push(e);
            }
        });
      };
    if (!Array.isArray(data)) checkFunc(data);
    else
      data.forEach(e => {
        checkFunc(e);
      });

    if (keyError.length > 0)
      Hoek.assert(
        false,
        `Key "${keyError.join(',')}" not found in model ${
          model.constructor.name
        }`
      );
    return HelperAdapter.parseStringKey(data);
  }

  static parseSort(src, model) {
    const objectSort = element => {
      let str;
      if (element.indexOf('-') >= 0) {
        str = {
          [helpers.String.toSnakeCase(element.replace('-', ''))]: -1
        };
      } else {
        str = {
          [helpers.String.toSnakeCase(element)]: 1
        };
      }
      HelperAdapter.checkKeyModelWithData(
        HelperAdapter.parseStringKey(str, {
          toCamelCase: true
        }),
        model
      );
      return str;
    };

    if (typeof src === 'string') return objectSort(src);
    let object = {};
    src.forEach(e => {
      object = Object.assign(object, objectSort(e));
    });
    return object;
  }

  static parseParams(params, opts = {}) {
    if (params) {
      const keys = Object.keys(params);
      if (keys.length > 0) {
        keys.forEach(e => {
          const current = params[e];
          if (
            !Array.isArray(current) &&
            typeof current === 'object' &&
            current !== null
          ) {
            if (current.in) {
              current.in = current.in.map(e => {
                if (helpers.Number.isNumber(e)) return e;
                if (ObjectID.isValid(e)) {
                  return ObjectID(e);
                }
                return e;
              });
              params[e] = { $in: current.in };
            }
            if (current.betweenDate) {
              const between = current.betweenDate,
                data = {
                  $gte: between.from,
                  $lt: between.to
                };
              params[e] = data;
            }

            if (current.not === true) {
              params[e] = {
                $not: params[e]
              };
            }
          }
        });
      }
    }
    return params;
  }

  static parseAggregate(params, src, model) {
    const includes = src.includes;
    // Hoek.assert(includes.length > 0, 'Aggregate includes not empty');
    let aggregate = [],
      paramQuery = params;
    const parseLookup = (storeOne, storeTwo) => {
      const collectionNameOne = storeOne.storeModel.model.collectionName,
        prefix = '_docs',
        parseColumnName = (index, column, two) => {
          let collectionNameTwo;
          if (two) collectionNameTwo = two.storeModel.model.collectionName;
          const columnName = column || storeOne.columns[index];

          return collectionNameTwo
            ? `${collectionNameTwo}${prefix}.${columnName}`
            : columnName;
        },
        lookup = {
          $lookup: {
            from: collectionNameOne,
            localField: parseColumnName(1, null, storeTwo),
            foreignField: storeOne.columns[0],
            as: `${collectionNameOne}${prefix}`
          }
        };
      if (storeOne.where && !helpers.Json.checkEmptyObject(storeOne.where)) {
        Object.keys(storeOne.where).forEach(e => {
          paramQuery = Object.assign(paramQuery, {
            [parseColumnName(null, e, storeOne)]: storeOne.where[e]
          });
        });
      }
      return lookup;
    };
    if (includes)
      includes.forEach(e => {
        if (e.joinMany && Array.isArray(e.joinMany) && e.joinMany.length > 0)
          e.joinMany.forEach((a, i) => {
            if (i === 0) {
              aggregate.push(parseLookup(a));
            } else {
              aggregate.push(parseLookup(a, e.joinMany[i - 1]));
            }
          });
        else aggregate.push(parseLookup(e));
      });

    if (!helpers.Json.checkEmptyObject(paramQuery))
      aggregate.push({
        $match: paramQuery
      });

    const integrateQuery = [
      'limit',
      'skip',
      'sort',
      'count',
      'countAggregate',
      'groupBy'
    ];
    Object.keys(src).forEach(e => {
      if (integrateQuery.includes(e)) {
        if (e === 'sort') {
          aggregate.push({
            $sort: HelperAdapter.parseSort(src[e], model)
          });
        } else if (e === 'countAggregate') {
          aggregate.push({
            $count: 'result'
          });
        } else if (e === 'groupBy') {
          const groupBy = HelperAdapter.parseGroupBy(src[e]);
          if (groupBy.group)
            aggregate.push({
              $group: groupBy.group
            });
          if (groupBy.project)
            aggregate.push({
              $project: groupBy.project
            });
        } else {
          aggregate.push({
            [`$${e}`]: src[e]
          });
        }
      }
    });

    return aggregate;
  }

  static parseGroupBy(src, opts = {}) {
    const result = {
        group: {},
        project: null
      },
      objecProject = {
        _id: 0
      };
    if (src.by) {
      result.group._id = `$${helpers.String.toSnakeCase(src.by)}`;

      if (src.sum) {
        const parseSum = e => {
          objecProject[e] = 1;
          e = helpers.String.toSnakeCase(e);
          if (e === 'count') {
            result.group[e] = { $sum: 1 };
          } else {
            result.group[e] = { $sum: `$${e}` };
          }
        };
        if (Array.isArray(src.sum)) {
          src.sum(e => {
            parseSum(e);
          });
        } else {
          parseSum(src.sum);
        }
      }
      result.project = objecProject;
      result.project[helpers.String.toSnakeCase(src.by)] = '$_id';
    }
    return result;
  }

  static parseProject(opts, model) {
    if (opts.fields && Array.isArray(opts.fields) && opts.fields.length > 0) {
      let fieldObject = {};
      opts.fields.forEach(e => {
        fieldObject = {
          [e === 'id' ? helpers.String.toSnakeCase(e) : _id]: 1
        };
      });
      if (model) HelperAdapter.checkKeyModelWithData(opts.fields, model);
      return fieldObject;
    }
    return {};
  }

  static parseStringKey(data, opts = {}) {
    if (!data) return data;
    let isObj;
    if (!Array.isArray(data)) {
      isObj = true;
      data = [data];
    }
    const funcResult = result => {
      if (isObj) return result[0];
      return result;
    };
    if (data.length === 0) return funcResult(data);
    const snakeKeyForObj = item => {
      if (helpers.Json.checkEmptyObject(item)) return item;
      const result = {};
      Object.keys(item).forEach(e => {
        const keyCurrent = helpers.String[
          opts.toCamelCase ? 'toCamelCase' : 'toSnakeCase'
        ](e);
        let dataCurrent = item[e];
        const parseKeyDefault = dataCurrent => {
          if (Array.isArray(dataCurrent)) {
            dataCurrent = dataCurrent.map(e => {
              if (typeof e === 'string') return e;
              return HelperAdapter.parseStringKey(dataCurrent, opts);
            });
            result[keyCurrent] = dataCurrent;
          } else if (
            typeof dataCurrent === 'object' &&
            !(dataCurrent instanceof Date)
          ) {
            dataCurrent = HelperAdapter.parseStringKey(dataCurrent, opts);
          }
          result[keyCurrent] = dataCurrent;
        };
        if (
          dataCurrent !== null &&
          typeof dataCurrent === 'object' &&
          ObjectID.isValid(dataCurrent) &&
          opts.toCamelCase
        ) {
          if (e === '_id') {
            result.id = dataCurrent;
          } else {
            result[keyCurrent] = dataCurrent;
          }
          // to snake case
        } else if (!opts.toCamelCase) {
          if (e === 'id' && !dataCurrent.$in) {
            result._id = ObjectID(dataCurrent);
          } else if (dataCurrent !== null && typeof dataCurrent === 'object') {
            if (
              (dataCurrent.type === 'objectId' && dataCurrent.id) ||
              (!helpers.Number.isNumber(dataCurrent) &&
                ObjectID.isValid(dataCurrent)) ||
              new RegExp('^[0-9a-fA-F]{24}$').test(dataCurrent)
            ) {
              /**
               * Check object id
               */
              result[keyCurrent] = ObjectID(dataCurrent.id);
            } else if (dataCurrent.$in) {
              if (e === 'id') result._id = dataCurrent;
              else result[keyCurrent] = dataCurrent;
            } else {
              parseKeyDefault(dataCurrent);
            }
          } else {
            parseKeyDefault(dataCurrent);
          }
        } else {
          parseKeyDefault(dataCurrent);
        }
      });
      return result;
    };
    return funcResult(
      data.map(e => {
        return snakeKeyForObj(e);
      })
    );
  }

  static formData(data, model, opts) {
    Hoek.assert(
      model,
      '[Helper Adapter Mongo] Function formData cannot null model'
    );

    if (model.formData) {
      if (Array.isArray(data) && data.length > 0) {
        data = data.map(e => {
          e = model.formData(e, opts);
          return e;
        });
      } else {
        data = model.formData(data, opts);
      }
    }
    return data;
  }

  static toKeyOptions(opts, model) {
    const result = {};
    if (opts.sort) {
      result.sort = HelperAdapter.parseSort(opts.sort, model);
    }
    return result;
  }
}

module.exports = HelperAdapter;
