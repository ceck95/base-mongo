module.exports = {
  BaseAdapter: require('./src/base/base-adapter'),
  BaseModel: require('./src/base/base-model')
};